FROM node:latest

# Create socialcops directory to hold code
WORKDIR /socialcops

COPY package*.json /socialcops/
RUN npm install
COPY . /socialcops

EXPOSE 8080

CMD [ "npm", "start" ]
