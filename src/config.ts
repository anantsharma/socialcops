/**
 * This file contains all the config
 * required by the application
 */

/**
 * White listed paths
 * Paths which don't require JWT for Authentication
 * Un-Protected paths
 */
const paths = [
  "/auth",
];

/**
 * JWT Related config
 */
const jwt = {
  options: {
    algorithm: "HS256",
    expiresIn: 3600,
    issuer: "Chipserver",
  },
  secret: "appsecret",
};

/**
 * Global DB Store
 */
const db = {
  getDbConnectionString: (dbName: string) => {
    return `mongodb://socialcops:socialcops@52.39.124.107:27017/${dbName}?ssl=false&authSource=admin`;
  },
};

/**
 * Config Object
 */
export const config = {
  appPort: 8000,
  db,
  jwt,
  paths,
};
