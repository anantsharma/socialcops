/**
 * Module dependencies.
 */
import * as moment from "moment";
import * as mongodb from "mongodb";
import { config } from "../../config";

/**
 * Create Mongo Client
 */
const MongoClient = mongodb.MongoClient;

/**
 * Create Connection with Database
 */
function createDbConnection(dbName: string): any {

    return new Promise((resolve, reject) => {

        MongoClient.connect(config.db.getDbConnectionString(dbName), (err: Error, db: any) => {

            if (err) {
                reject(err);
                return;
            }

            resolve(db);
        });

    });
}

export function insert(dbName: string, collectionName: string, data: any) {

    return new Promise(async (resolve, reject) => {

        try {

            /**
             * Get DB
             */
            const db = await createDbConnection(dbName);

            /**
             * Get Collection
             */
            const collection = db.collection(collectionName);

            /**
             * Attach timestamp
             */
            if (Array.isArray(data)) {
                const _timestamp = moment.utc().valueOf();
                data.forEach((d) => {
                    d._created_at = _timestamp;
                });
            }

            /**
             * Insert list of documents
             */
            collection.insertMany(data, (err: Error, result: any) => {

                db.close();

                if (err) {
                    reject(err);
                    return;
                }

                resolve(result);
            });

        } catch (e) {

            reject(e);

        }

    });
}

export function updateMany(dbName: string, collectionName: string, query: any, data: any) {

    return new Promise(async (resolve, reject) => {

        try {

            /**
             * Get DB
             */
            const db = await createDbConnection(dbName);

            /**
             * Get Collection
             */
            const collection = db.collection(collectionName);

            /**
             * Attach timestamp
             */
            const _timestamp = moment.utc().valueOf();
            data._last_updated_at = _timestamp;

            /**
             * Update list of documents
             */
            collection.updateMany(query, {$set: data}, (err: Error, result: any) => {

                db.close();

                if (err) {
                    reject(err);
                    return;
                }

                resolve(result);
            });

        } catch (e) {

            reject(e);

        }

    });
}
