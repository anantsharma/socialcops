/**
 * This file contains the common utils
 * required by the application
 */

export async function delay(time: number) {

    return new Promise((resolve, reject) => {
        setTimeout(resolve, time);
    });

}
