/**
 * This file contains the code required to
 * log the application data
 */

import {v4 as uuidv4} from "uuid";
import {Consoler} from "./consoler";

/**
 * Logger Class Declaration
 */
export class Logger {

    public _data: any = {};

    constructor() {
        this._data._loggerId = uuidv4();
    }

    public set(key: string, value: any) {
        this._data[key] = value;
    }

    public log(data: string) {
        const consoler = new Consoler();
        consoler.console(data, this._data, "log");
    }

    public trace(data: string) {
        const consoler = new Consoler();
        consoler.console(data, this._data, "trace");
    }

    public error(data: Error) {
        const consoler = new Consoler();
        consoler.console(data.message, this._data, "error");
    }

    public info(data: string) {
        const consoler = new Consoler();
        consoler.console(data, this._data, "info");
    }

}
