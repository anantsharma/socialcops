/**
 * This file contains the code required
 * to console data into log stream
 */

import * as _ from "lodash";
import * as moment from "moment";
import { v4 as uuidv4 } from "uuid";
import { LocalDb } from "./logDb";

export class Consoler {

    public pushData = {};
    public localDb: any;

    constructor() {
        this.pushData = {
            _consolerId: uuidv4(),
            _timestamp: moment.utc().valueOf(),
        };

        this.localDb = new LocalDb();
    }

    public console(data: string, _data: any, type: any) {

        _.forOwn(_data, (value, key) => {
            console.log(`${_.startCase(key)}: ${value}`);
        });

        switch (type) {
            case "trace": console.trace(data); break;
            case "error": console.error(data); break;
            case "info": console.info(data); break;
            default: console.log(data);
        }
        console.log("\n");

        this.pushData = _.merge(this.pushData, _data, {
            body: data,
        });

        this.insertIntoLocalStore(type, this.pushData);
    }

    public async insertIntoLocalStore(type: string, data: any) {
        await this.localDb.insert(type, data);
    }
}
