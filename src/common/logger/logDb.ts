/**
 * This file contains the code required to
 * push data into local db store & global db store
 */
import * as _ from "lodash";
import * as Datastore from "nedb";
import { v4 as uuidv4 } from "uuid";
import * as dbService from "../db-service/index";
import { delay } from "../utils";

let initialized = false;

const dbList = ["log", "trace", "error", "info"];

/**
 * This function initializes
 * local logger db stores
 */
function initializeLoggerDb() {
    const loggerDatastores: any = {};
    _.forEach(dbList, (db) => {
        loggerDatastores[db] = new Datastore({ filename: `log/${db}.db`, autoload: true });
    });
    initialized = true;
    return loggerDatastores;
}
const loggerDb: any = initializeLoggerDb();

/**
 * This class contains the consumable
 * function(s) to log data.
 */
export class LocalDb {

    public insert(db: any, data: any) {
        return new Promise(async (resolve, reject) => {

            if (!initialized) {
                await delay(1000);
            }

            loggerDb[db].insert(data, (err: Error, doc: any) => {
                if (err) {
                    reject(err);
                }
                resolve(doc);
            });

        });
    }
}

/**
 * Flush data to global store
 */
function flush(type: any) {
    /**
     * Get all documents from datastore
     */
    loggerDb[type].find({}, async (err: Error, docs: any) => {

        if (docs.length === 0) {
            return;
        }

        /**
         * Specify Log Type
         */
        docs = _.map(docs, (doc: any) => {
            doc.consoleType = type;
            doc.localId = doc._id;
            delete doc._id;
            return doc;
        });

        /**
         * Push to mongo store
         */
        const insResult = await dbService.insert("socialcops", "logs", docs);

        /**
         * If inserted, remove entries from local db
         */
        _.forEach(docs, (doc: any) => {
            loggerDb[type].remove({
                _id: doc.localId,
            }, {multi: true});
        });

    });

}

/**
 * Flush on regular intervals
 */
(function initFlush() {
    setInterval(() => {
        _.forEach(dbList, flush);
    }, 1000 * 5);
})();
