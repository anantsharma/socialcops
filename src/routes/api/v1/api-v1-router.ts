/**
 * Import Dependencies
 */
import * as express from "express";

/**
 * Initialize Router
 */
const router = express.Router();

/**
 * Import Routes
 */
import imageProcessingRouter from "./image-processing/router";
import jsonPatchRouter from "./json-patch/router";

/**
 * Bind Routes
 */
router.use("/json-patch", jsonPatchRouter);
router.use("/img", imageProcessingRouter);

/**
 * Export Module
 */
export default router;
