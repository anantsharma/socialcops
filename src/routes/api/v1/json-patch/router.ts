/**
 * Import Dependencies
 */
import * as express from "express";
import {
    has,
    isArray,
    isObjectLike,
} from "lodash";
import { Logger } from "../../../../common/logger/logger";
import { PatchBox } from "./module";

/**
 * Initialize Router
 */
const router = express.Router();

/**
 * Logger Config
 */
const logger = new Logger();
logger.set("filepath", __filename);

/**
 * Bind Routes
 */
router.post("/", (req: express.Request, res: express.Response, next: express.NextFunction) => {

    /**
     * Set Logger Params
     */
    logger.set("method", "POST");

    /**
     * Ensure document is present
     */
    if (!has(req.body, "document")) {
        res.status(400).json({
            error: "JSON document is required",
        });
        next();
        return;
    }

    /**
     * Ensure Patch is present
     */
    if (!has(req.body, "patch")) {
        res.status(400).json({
            error: "JSON patch is required",
        });
        next();
        return;
    }

    /**
     * Get document & patch from payload
     */
    const {
        document,
        patch,
    } = req.body;

    /**
     * Ensure document is a JSON Object
     */
    if (!(isObjectLike(document) && !isArray(document))) {
        res.status(400).json({
            error: "A valid JSON document is required",
        });
        next();
        return;
    }

    /**
     * Ensure patch is an Object
     */
    if (!isObjectLike(patch)) {
        res.status(400).json({
            error: "A valid patch is required",
        });
        next();
        return;
    }

    /**
     * Create patch box
     */
    const patchBox = new PatchBox(document, patch);

    /**
     * Apply Patch
     */
    try {
        const patched = patchBox.apply();
        res.send(patched);
    } catch (e) {
        logger.trace(e);
        res.status(400).json({
            error: e,
        });
    }

});

/**
 * Export Module
 */
export default router;
