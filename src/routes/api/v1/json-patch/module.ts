/**
 * This file contains the functional code
 * pertaining to json patch
 */

/**
 * Module Dependencies
 */
import { apply as applyJsonPatch } from "json-patch";

export class PatchBox {

    public document: any;
    public patch: any;

    constructor(document: any, patch: any) {
        this.document = document;
        this.patch = patch;
    }

    public apply() {

        const {
            document,
            patch,
        } = this;

        return applyJsonPatch(document, patch);
    }
}
