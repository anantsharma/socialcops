/**
 * This module contains the code required
 * to check json patch module
 */

/**
 * Import Dependencies
 */
import * as chai from "chai";
import { PatchBox } from "../module";

const should = chai.should();

describe("Patch Box", () => {

    const document = {
        baz: "qux",
        foo: "bar",
    };

    const patch = [
        { op: "replace", path: "/baz", value: "boo" },
        { op: "add", path: "/hello", value: ["world"] },
        { op: "remove", path: "/foo"},
    ];

    const result = {
        baz: "boo",
        hello: ["world"],
    };

    const patchBox = new PatchBox(document, patch);

    it("Constructor :: Should be an object", (done) => {
        chai.expect(patchBox).to.be.an("Object");
        done();
    });

    it("Apply :: Should apply patch to document", (done) => {
        chai.expect(patchBox.apply()).to.have.all.keys("baz", "hello");
        done();
    });

});
