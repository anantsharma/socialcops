/**
 * Import Dependencies
 */
import * as bodyParser from "body-parser";
import * as express from "express";
import { has } from "lodash";
import { Logger } from "../../../../common/logger/logger";
import { ImageProcessor } from "./module";

/**
 * Initialize Router
 */
const router = express.Router();

/**
 * Logger Config
 */
const logger = new Logger();
logger.set("filepath", __filename);

/**
 * Bind Routes
 */
router.post("/", async (req: express.Request, res: express.Response, next: express.NextFunction) => {

    /**
     * Set Logger Params
     */
    logger.set("method", "POST");

    /**
     * Ensure image url is present
     */
    if (!has(req.body, "url")) {
        res.status(400).json({
            error: "Image URL is required",
        });
        next();
        return;
    }

    /**
     * Fetch image url from request body
     */
    const {
        url,
    } = req.body;

    logger.set("url", url);

    /**
     * Create instance of Image Processor
     */
    const imageProcessor = new ImageProcessor(url);

    try {

        /**
         * Fetch image from URL
         */
        await imageProcessor.fetch();

        /**
         * Resize Image
         */
        const resized = await imageProcessor.resize();

        /**
         * Send Response
         */
        res.set("Content-Type", "image/png");
        res.send(resized);

    } catch (e) {
        logger.trace(e);
        res.status(400).json({
            msg: e,
            status: "error",
        });
    }

});

/**
 * Export Module
 */
export default router;
