/**
 * This module contains the code required
 * to check Image Processor Module
 */

/**
 * Import Dependencies
 */
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { setTimeout } from "timers";
import { ImageProcessor } from "../module";

const _IMAGE_URL = "https://www.gettyimages.com.au/gi-resources/images/Embed/new/embed2.jpg";
const _NO_IMAGE_URL = "https://www.gettyimages.com.au/gi-resources/images/Embed/new/embed2.png";
const should = chai.should();

chai.use(chaiAsPromised);

describe("Image Processor", () => {

    const imageProcessor = new ImageProcessor(_IMAGE_URL);
    const noImageProcessor = new ImageProcessor(_NO_IMAGE_URL);

    it("Constructor :: Should be an object", (done) => {
        chai.expect(imageProcessor).to.be.an("Object");
        done();
    });

    it("Fetch :: Should resolve", (done) => {
        imageProcessor.fetch().should.be.fulfilled;
        done();
    });

    it("Fetch :: Should be rejected", (done) => {
        noImageProcessor.fetch().should.be.rejected;
        done();
    });

    it("Resize :: Should resolve", (done) => {
        imageProcessor.fetch().then(() => {
            imageProcessor.resize().should.be.fulfilled;
            done();
        });
    });

    it("Resize :: Should be rejected", (done) => {
        noImageProcessor.resize().should.be.rejected;
        done();
    });

});
