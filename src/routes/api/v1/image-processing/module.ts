/**
 * This file contains the functional code
 * pertaining to image processing
 */

/**
 * Module Dependencies
 */
import * as jimp from "jimp";

export class ImageProcessor {

    public url: string;
    public image: jimp;

    constructor(url: string) {
        this.url = url;
    }

    public fetch() {

        const {
            url,
        } = this;

        return new Promise((resolve, reject) => {

            jimp.read(url, (err: Error, image) => {
                if (err) {
                    reject(err);
                    return;
                }

                if (image === undefined) {
                    reject("Unable to fetch image from URL");
                    return;
                }

                this.image = image;
                resolve();
            });
        });
    }

    public resize() {

        return new Promise<Buffer>((resolve, reject) => {

            try {

                this.image
                    .resize(50, 50)
                    .getBuffer(jimp.MIME_PNG, (err: Error, buffer: Buffer) => {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve(buffer);
                    });

            } catch (e) {
                reject(e);
            }

        });
    }

}
