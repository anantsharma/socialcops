import * as express from "express";
import * as jwt from "jsonwebtoken";
import { has } from "lodash";
import * as moment from "moment";
import { config } from "../../config";

const router = express.Router();

router.post("/", (req: express.Request, res: express.Response, next ?: express.NextFunction) => {

    if (!has(req.body, "username")) {
        res.status(400).json({
            error: "Username is required",
        });
        next();
        return;
    }
    if (!has(req.body, "password")) {
        res.status(400).json({
            error: "Password is required",
        });
        next();
        return;
    }

    /**
     * Sign JWT
     */
    const token = jwt.sign({}, config.jwt.secret, config.jwt.options);

    /**
     * Attach additonal props and send JWT
     */
    res.status(200).json({
        access_token: token,
        expires_at: moment().add(config.jwt.options.expiresIn, "seconds").format("x"),
        expires_in: config.jwt.options.expiresIn,
        token_type: "bearer",
    });

});

export default router;
