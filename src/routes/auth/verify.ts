import * as express from "express";
import * as jwt from "jsonwebtoken";
import * as _ from "lodash";
import { Logger } from "../../common/logger/logger";
import { config } from "../../config";

/**
 * Logger Config
 */
const logger = new Logger();
logger.set("filepath", __filename);

export default (req: any, res: any, next ?: express.NextFunction) => {

    /**
     * If the requested path (req.path) is not an un-protected path
     * Or app is not running in test mode
     */
    if (! _.includes(config.paths, req.path)) {

        let token;

        try {

            /**
             * Check for Authorization Token
             */
            if (req.headers.authorization && req.headers.authorization.split(" ")[0] === "Bearer") {
                token = req.headers.authorization.split(" ")[1];
            } else if (req.query && req.query.token) {
                token = req.query.token;
            }

        } catch (e) {
            logger.trace(e);
            res.status(401).send({
                error: "Authorization Token is required",
                status: "error",
            });
            return;
        }

        /**
         * Verify JWT Token
         */
        jwt.verify(token, config.jwt.secret, config.jwt.options, (err, decoded) => {
            if (err) {
                logger.trace(err.message);
                res.status(401).send({
                    error: "A valid authorization token is required",
                    status: "error",
                });
                return;
            }

            next();
        });

    } else {
        next();
    }
};
