/**
 * This file contains the code required
 * to bootstrap the server.
 */

/**
 * Module dependencies
 */
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as morgan from "morgan";
import { Logger } from "./common/logger/logger";
import { delay } from "./common/utils";
import { config } from "./config";

/**
 * API Routes
 */
import apiRouter from "./routes/api/api-router";
import authRouter from "./routes/auth/router";
import authVerify from "./routes/auth/verify";

const app = express();

/**
 * Initialize Middlewares
 */
app.use(morgan("dev"));
app.use(bodyParser.json({
  limit: "50mb",
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: "50mb",
}));
app.use(cors());
app.use(authVerify);

/**
 * Routes
 */
app.use("/auth", authRouter);
app.use("/api", apiRouter);

/**
 * Initialize Application Server
 */
(async function init() {
  /**
   * Delay Application startup for internal
   * processes to bootstrap
   */
  await delay(1000);

  /**
   * Logger Config
   */
  const logger = new Logger();
  logger.set("filepath", __filename);
  logger.log("[*] Initializing Application...");

  /**
   * Initialize API Server
   */
  app.listen(config.appPort, (err: Error) => {
    if (err) {
      logger.trace(err.message);
      return;
    }
    logger.log(`[*] Server started on port ${config.appPort}`);
  });
}());
