# README #

This module contains the code required for APIs for Social Cops.

## Installation
    git clone https://anantsharma@bitbucket.org/anantsharma/socialcops.git
    npm install

## Start
This application uses `typescript` and needs to be transpiled. The below script takes care of building & spawning the process. 

    npm start

## Build
This application uses `typescript` and needs to be built.

    npm run build

## Test
The application uses mocha & chai as test suite. Also, istanbul.js is used for code coverage and report is generated in `coverage` folder.

    npm run test

## Lint
The application uses `tslint` for js lint checks.

    npm run lint